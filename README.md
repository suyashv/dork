# dork

__Minimalistic__, __Opinionated__ templates, installed from the command line, to avoid doin the same shit every time.

## Usage

The templates (dorks), are stored in branches in this repo. The dorks are also cached locally in ```~/.dork```.

First

```shell
npm i -g suyash/dork
```

To set up the command line tool. Now, to get the base template,

```shell
dork 'dork_name' 'location(absolute/relative)'
```

Example,

```shell
dork base my-project
```

Location defaults to `cwd`.

All dorks can be seen in [Active branches](https://github.com/suyash/dork/branches/active).
