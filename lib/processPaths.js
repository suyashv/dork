var getHome = require('./getHome'),
    path = require('path');

var isAbsolutePath = function (location) {
  return location.match(/^\//g);
};

var isHomeRelative = function (location) {
  return location.match(/^~/g);
};

module.exports = function (location, base) {
  if (isAbsolutePath(location)) {
    return location;
  }

  if (isHomeRelative(location)) {
    return location.replace(/^~/, getHome());
  }

  if (!base) {
    throw new Error('base required for relative paths');
  }

  return path.join(base, location);
};
