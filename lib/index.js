var download = require('download-github-repo'),
    exec = require('child_process').exec,
    filter = require('./filter'),
    fs = require('fs-extra'),
    GitHub = require('github'),
    path = require('path'),
    pkg = require('../package.json');


/**
 * @constructor
 * @param cache: the location of dork cache
 * @param base: optional, used for resolving if cache id a relative path
 */

var Dork = module.exports = function (location) {
  this.location = location;
  this.user = 'suyash';
  this.repo = 'dork';
  this.github = new GitHub({
    version: '3.0.0'
  });
};


/**
 * @function version
 */

Dork.version = function () {
  return pkg.version;
};


/**
 * @function installLocation
 */

Dork.prototype.installLocation = function (name) {
  return path.join(this.location, name);
};


/**
 * @function isInstalled
 * checks if a dork of the specified name is cached
 */

Dork.prototype.isInstalled = function (name, cb) {
  fs.exists(this.installLocation(name), cb);
};


/**
 * @function exists
 * checks if it is a valid dork, period
 */

Dork.prototype.exists = function (name, cb) {
  var self = this;

  self.github.repos.getBranch({
    user: self.user,
    repo: self.repo,
    branch: name
  }, cb);
};


/**
 * @function install
 * install a dork if not already installed
 */

Dork.prototype.install = function (name, cb) {
  var self = this;

  self.isInstalled(name, function (exists) {
    if (exists) {
      return cb(new Error('dork#' + name + ' already installed'));
    }
    download(self.user + '/' + self.repo + '#' + name, self.installLocation(name), cb);
  });
};


/**
 * @function hasRecipe
 */

Dork.prototype.hasRecipe = function (name, cb) {
  var self = this;
  self.isInstalled(name, function (stat) {
    if (!stat) {
      return cb(stat);
    }
    fs.exists(path.join(self.installLocation(name), 'dork.sh'), cb);
  });
};


/**
 * @function execRecipe
 */

Dork.prototype.execRecipe = function (name, cb) {
  var self = this;

  var location = self.installLocation(name);
  var cmd = 'cd ' + location + ' && . ' + location + '/dork.sh';

  var process = exec(cmd, function (err) {
    if (err) {
      return cb(err);
    }
    cb(null);
  });

  process.stdout.on('data', function (message) {
    console.log(message);
  });
};


/**
 * @function setup
 * setup an installed dork in the specified folder
 */

Dork.prototype.setup = function (name, location, cb) {
  var self = this;

  self.isInstalled(name, function (exists) {
    if (!exists) {
      return cb(new Error('dork must be installed locally to be setup'));
    }
    fs.copy(self.installLocation(name), location, filter, cb);
  });
};
