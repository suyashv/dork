/* globals describe, it */

var expect = require('chai').expect,
    filter = require('../../lib/filter');


module.exports = function () {

  describe('filter', function () {

    it('filters out READMEs', function () {

      expect(filter('README.md')).to.equal(false);
      expect(filter('README.markdown')).to.equal(false);

    });

    it('returns true otherwise', function () {

      expect(filter('file.ext')).to.equal(true);

    });

  });

};
