/**
 * Filter to be used while copying
 */

module.exports = function (name) {
  return !name.match(/README\..*/g) && !name.match(/dork\.sh$/g);
};
