#!/usr/bin/env node

/**
 * dork CLI
 *
 * Operation:
 *
 * dork [dork-name] [location (optional, defaults to cwd)]
 *
 * flags:
 *
 * -h, --help       show this message
 * -V, --version    version
 */

var chalk = require('chalk'),
    Dork = require('./'),
    processPaths = require('./processPaths'),
    program = require('commander');

/**
 * commander
 */

program
  .version(Dork.version())
  .usage('[dork-name] [location]')
  .parse(process.argv);

if (!program.args.length) {
  program.help();
}

var name = program.args.shift();
var location = program.args.shift() || process.cwd();

/**
 * locals
 */

var cache = processPaths(process.env.DORK_CACHE || '~/.dork');
var installLocation = processPaths(location, process.cwd());
var dork = new Dork(cache);

/**
 * methods to log to console
 */

var log = function (message) {
  console.log('\n' + message);
};

var logInfo = function (message) {
  log(chalk.blue(message));
};

var logSuccess = function (message) {
  log(chalk.green(message));
};

var logWarning = function (message) {
  log(chalk.yellow(message));
};

var logError = function (message) {
  log(chalk.red(message));
};

/**
 * sets up a dork
 */

var setup = function () {
  logInfo('setting up dork#' + name + ' in ' + installLocation);

  dork.setup(name, installLocation, function (err) {
    if (err) {
      throw err;
    }
    logSuccess('dork#' + name + ' set up in ' + installLocation);
  });
};

/**
 * start
 */

logInfo('Checking if dork#' + name + ' is installed');

dork.isInstalled(name, function (exists) {

  if (exists) {
    logSuccess('dork#' + name + ' installed locally');
    return setup();
  }

  logWarning('dork#' + name + ' not installed locally');

  logInfo('checking if dork#' + name + ' exists');

  dork.exists(name, function (err, res) {
    if (err) {
      throw err;
    }
    logSuccess('dork#' + name + ' exists on GitHub');

    logInfo('installing dork#' + name);

    dork.install(name, function (err) {
      if (err) {
        throw err;
      }
      logInfo('installed dork#' + name);

      logInfo('checking if dork#' + name + ' has a recipe');

      dork.hasRecipe(name, function (stat) {

        if (!stat) {
          logInfo('dork#' + name + ' doesn\'t have a recipe');
          return setup();
        }

        logInfo('dork#' + name + ' has a recipe, running it now.');
        dork.execRecipe(name, function (err) {

          if (err) {
            throw err;
          }
          setup();

        });
      });
    });
  });
});
