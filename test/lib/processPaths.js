/* globals describe, it */

var expect = require('chai').expect,
    processPaths = require('../../lib/processPaths');


module.exports = function () {

  describe('processPaths', function () {

    it('should work for absolute paths', function () {

      expect(processPaths('/tmp/something')).to.equal('/tmp/something');

    });

    it('should work for relative paths', function () {

      expect(processPaths('something', process.cwd())).to.equal(process.cwd() + '/something');

    });

    it('should work for home-based paths', function () {

      expect(processPaths('~/tmp/something')).to.equal(process.env.HOME + '/tmp/something');

    });

    it('should throw an error for relative path without a base specified', function () {

      expect(processPaths.bind(null, 'something')).to.throw(Error, /base required/);

    });

  });

};
