/* globals describe, it, beforeEach, afterEach */

var Dork = require('../../'),
    expect = require('chai').expect,
    fs = require('fs-extra'),
    path = require('path');


module.exports = function () {

  it('has a method that spits out the program version', function () {

    expect(Dork.version()).to.not.be.null();

  });

  describe('API', function () {

    var dork = null,
        fixture = path.join(process.cwd(), 'test/fixture'),
        cachePath = path.join(fixture, '/cache'),
        setupPath = path.join(fixture, '/setup');

    beforeEach(function () {

      dork = new Dork(cachePath);

    });

    it('can check for existing dorks', function (done) {

      this.timeout(5000);

      dork.exists('stat', function (err) {

        expect(err).to.be.null();

        done();

      });

    });

    it('gives error for non existing dorks', function (done) {

      this.timeout(5000);

      dork.exists('poop', function (err) {

        expect(err).to.not.be.null();

        done();

      });

    });

    it('can install dorks', function (done) {

      this.timeout(5000);

      dork.install('stat', function (err) {

        if (err) {
          throw err;
        }

        fs.exists(path.join(cachePath, 'stat'), function (stat) {

          expect(stat).to.be.true();

          done();

        });

      });

    });

    it('can setup dorks', function (done) {

      this.timeout(5000);

      dork.setup('stat', setupPath, function (err) {

        done();

      });

    });

    afterEach(function () {

      fs.delete(fixture);

    });
  });
};
